( function($) {

/* ----------------------------------------------------------------------
 __      __       _____   _____            ____   _       ______   _____
 \ \    / //\    |  __ \ |_   _|    /\    |  _ \ | |     |  ____| / ____|
  \ \  / //  \   | |__) |  | |     /  \   | |_) || |     | |__   | (___
   \ \/ // /\ \  |  _  /   | |    / /\ \  |  _ < | |     |  __|   \___ \
	\  // ____ \ | | \ \  _| |_  / ____ \ | |_) || |____ | |____  ____) |
	 \//_/    \_\|_|  \_\|_____|/_/    \_\|____/ |______||______||_____/

---------------------------------------------------------------------- */




/* ----------------------------------------------------------------------
  ______  _    _  _   _   _____  _______  _____  ____   _   _   _____
 |  ____|| |  | || \ | | / ____||__   __||_   _|/ __ \ | \ | | / ____|
 | |__   | |  | ||  \| || |        | |     | | | |  | ||  \| || (___
 |  __|  | |  | || . ` || |        | |     | | | |  | || . ` | \___ \
 | |     | |__| || |\  || |____    | |    _| |_| |__| || |\  | ____) |
 |_|      \____/ |_| \_| \_____|   |_|   |_____|\____/ |_| \_||_____/

---------------------------------------------------------------------- */

/* --- Behaviors --- */
window.b = window.b || {};
/* --- Functions --- */
window.f = window.f || {};

/* --- Basic behaviors --- */
window.b.basic = {

	attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this;

		/* ...Plugin & other initializations... */
	}
};

/* --- Tooltip --- */
window.b.tooltip = {

	attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this,
			$topics = $('.js_tooltip', $context);
		
		self._create($topics);

		self._autoplay($topics);

		self._interact($topics);
	},
	_create: function($topics) {

		$.each($topics, function(i) {

			var $tooltip = $('<div>').addClass('topic_tooltip').css('color', $(this).attr('data-color')).text($(this).attr('data-tooltip'));

			$(this).prepend($tooltip);
		});
	},
	_autoplay: function($topics, $index) {

		var $index = ('number' === typeof $index ? $index : 0 );
		
		$topics.eq($index).addClass('active');

		var $autoplay = setInterval(function() {

			var $active_topic = $topics.filter('.active'),
				$next_topic = 0 < $active_topic.next().length ? $active_topic.next() : $topics.eq(0);

			$active_topic.removeClass('active');
			$next_topic.addClass('active');

		}, 1200);
	},
	_interact: function($topics) {

		$topics.hover(

			function () {

				$(this).addClass('selected');
				$topics.parent().addClass('interaction');
			},
			function () {

				$(this).removeClass('selected');
				$topics.parent().removeClass('interaction');
			}
		);
	}
};

/* --- Modal --- */
window.b.modal = {

    attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this;
	
		$context.on('click', '.js_modal, .js_closemodal', function(e) {

			e.preventDefault();

			var $modal = $('.modal');

			if ($modal.hasClass('open')) {

				self._close($modal, $size);
			}
			else {

				var $content_id = $(this).attr('data-modalcontent') ? $(this).attr('data-modalcontent') : $('[data-modalcontent]:first-child', '.modal_content').attr('data-modalcontent'), 
					$content = $('[data-modalcontent*="' + $content_id + '"]', '.modal_content'),
					$size = $('[data-modalcontent="' + $content_id + '"]', '.modal_content').attr('data-modalsize');

				self._open($modal, $size, $content);
			}
		});
	},
    _open: function($modal, $size, $content) {
        
		var $modal_overlay = $('.modal_overlay', $modal),
			$modal_wrap = $('.modal_wrap', $modal),
			$modal_box = $('.modal_box', $modal);
		
		$modal.attr('data-modalsize', $size);
		$modal_box.html($content.clone());

		TweenMax.set($modal_overlay, {autoAlpha:0});
		TweenMax.set($modal_wrap, {y:100, autoAlpha:0});
		TweenMax.set($modal, {className:'+=open'});
		TweenMax.to($modal_overlay, 0.3, {autoAlpha:0.9});
		TweenMax.to($modal_wrap, 0.3, {y:0, autoAlpha:1, scale:1});
    },
    _close: function($modal) {
        
		var $modal_overlay = $('.modal_overlay', $modal),
			$modal_wrap = $('.modal_wrap', $modal),
			$modal_box = $('.modal_box', $modal);

		TweenMax.to($modal_overlay, 0.3, {autoAlpha:0});
		TweenMax.to($modal_wrap, 0.3, {y:100, autoAlpha:0, onComplete:function(){

			TweenMax.set($modal, {className:'-=open'});
			$modal.attr('data-modalsize', '');
			$modal_box.empty();
		}});
    }
};

/* --- Form handler --- */
window.b.form = {

	attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this;
		
		$context.on('submit', 'form', function(e) {
		
			e.preventDefault();

			self._send($(this));
		});
	},
	_send: function($form) {

		var $form_box = $form.closest('.form_box'),
			$form_data = $form.serialize(),
			$form_noty_success = $('.form_noty[data-state="success"]', $form_box),
			$form_noty_error = $('.form_noty[data-state="error"]', $form_box);
		
		// if ($form_noty_error.is(':visible')) $form_noty_error.slideUp(500);

		// $.ajax({
		// 	url : '/ctrl/handler.php',
		// 	type : 'POST',
		// 	data : $form_data,
		// 	cache : false,
		// 	dataType : 'json',
		// 	success : function(data, textStatus, jqXHR) {

		// 		if (data.status == 'success') {

		// 			$form.slideUp(500, function() {

		// 				$form_noty_success.slideDown(500);
		// 			});
		// 		}
		// 		else {

		// 			$form_noty_error.slideDown(500);
		// 		}
		// 	},
		// 	error : function(jqXHR, textStatus, errorThrown) {

		// 		$form_noty_error.slideDown(500);
		// 	}
		// });

		// There is no handler, so just fake it
		$form.slideUp(500, function() {

			$form_noty_success.slideDown(500);
		});
	}
};

/* --- Dropdown menu --- */
window.b.dropdown = {

	attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this;

		$('.js_dropdown', $context).hover(

			function () {

				var $drop_btn = $(this),
					$dropdown = $('.dropdown', $drop_btn),
					$dropdown_box = $dropdown.children('ul');

				self._open($drop_btn, $dropdown, $dropdown_box, 50);
			},
			function () {

				var $drop_btn = $(this),
					$dropdown = $('.dropdown', $drop_btn);

				self._close($drop_btn, $dropdown, 50);
			}
		);
	},
    _open: function($drop_btn, $dropdown, $dropdown_box, $drop_delay) {
		
		setTimeout(function() {

			TweenMax.set($drop_btn, {className:'+=open'});
			TweenMax.set($dropdown, {className:'+=open', visibility:'visible'});
			TweenMax.to($dropdown, 0.2, {height:$dropdown_box.outerHeight()});

		}, 'undefined' !== typeof $drop_delay ? $drop_delay : 0 );
	},
    _close: function($drop_btn, $dropdown, $drop_delay) {

		setTimeout(function() {

			TweenMax.set($drop_btn, {className:'-=open'});
			TweenMax.set($dropdown, {className:'-=open'});
			TweenMax.to($dropdown, 0.2, {height:0, onComplete:function(){

				TweenMax.set($dropdown, {visibility:'hidden'});
			}});

		}, 'undefined' !== typeof $drop_delay ? $drop_delay : 0 );
	}
};

/* --- Compact menu --- */
window.b.compact = {

	attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this;

		$context.on('click', '.js_compact', function(e) {

			e.preventDefault();

			var $compact_button = $(this),
				$compact_menu = $('.menu');

			if ($compact_button.hasClass('open')) {

				self._close($compact_button, $compact_menu);
			}
			else {

				self._open($compact_button, $compact_menu);
			}
		});
	},
    _open: function($button, $menu) {
		
		$button.addClass('open');
		$menu.addClass('open');
	},
    _close: function($button, $menu) {

		$button.removeClass('open');
		$menu.removeClass('open');
	}
};

/* --- Content load --- */
window.b.loadin = {

	attach: function($context) {

        this._init($context);
    },
    _init: function($context) {

		var self = this,
			$content = $('.js_loadin');

		$(window).on('load resize scroll', function() {
			self._check($content);
		});
	},
	_check: function($content) {

		var self = this;

		$.each($content, function() {
			
			if (self._visible($(this))) {
				$(this).addClass('visible');
			}
			else {
				$(this).removeClass('visible');
			}
		});
	},
    _visible: function($element) {

		var $partial = $element.data('partial') || true;

		var $view_top = $(window).scrollTop(),
			$view_bottom = $view_top + $(window).height(),
			$element_top = $element.offset().top,
			$element_bottom = $element_top + $element.height(),
			$compare_top = $partial === true ? $element_bottom : $element_top,
			$compare_bottom = $partial === true ? $element_top : $element_bottom;
		
		return (($compare_bottom <= $view_bottom) && ($compare_top >= $view_top));
	}
};

/* --- Initialize behaviors --- */
window.b.init = function() {

	var $context = $context ? $($context) : $(document);

	/* --- Basic behaviors --- */
	window.b.basic.attach($context);

	/* --- Tooltip --- */
	window.b.tooltip.attach($context);

	/* --- Modal --- */
	window.b.modal.attach($context);

	/* --- Form --- */
	window.b.form.attach($context);

	/* --- Dropdown menu --- */
	window.b.dropdown.attach($context);

	/* --- Compact menu --- */
	window.b.compact.attach($context);

	/* --- Content load --- */
	window.b.loadin.attach($context);
};

/* ----------------------------------------------------------------------
  _____   ______            _____ __     __
 |  __ \ |  ____|    /\    |  __ \\ \   / /
 | |__) || |__      /  \   | |  | |\ \_/ /
 |  _  / |  __|    / /\ \  | |  | | \   /
 | | \ \ | |____  / ____ \ | |__| |  | |
 |_|  \_\|______|/_/    \_\|_____/   |_|

---------------------------------------------------------------------- */

$(document).ready(function () {
	
	window.b.init();
});

} ) ( jQuery );