'use strict';

// Dependencies
const   gulp = require('gulp'),
        plugin = require('gulp-load-plugins')(); // concat, filter, rename, eslint, uglify, sass, autoprefixer, tinypng-compress, sourcemaps, browser-sync, live-server

// Config
const   config = {
            url: 'localhost:8080', // Value: proxy an existing vhost
            tinypngKey: 'C-J3Ecd4kw0ISXqlriNpn3j7_0AeA82c', // Limit: 500 image / month
            autoprefixer: ['last 2 versions'] // Deault: > 1%, last 2 versions, Firefox ESR
        };

// Paths
const   path_dev = './dev',
        path_web = './web',
        paths = {
            js: {
                dir: path_dev + '/js/',
                l_src: path_dev + '/js/libs/*.js',
                p_src: path_dev + '/js/plugins/*.js',
                s_src: path_dev + '/js/scripts/*.js',
                dest: path_web + '/js'
            },
            css: {
                dir: path_dev + '/scss/',
                src: path_dev + '/scss/**/*.scss',
                dest: path_web + '/css'
            },
            font: {
                dir: path_dev + '/font/',
                src: path_dev + '/font/*.{ttf,woff,woff2,eot,svg}',
                dest: path_web + '/font'
            },
            img: {
                dir: path_dev + '/img/',
                src: path_dev + '/img/**/*',
                dest: path_web + '/img'
            },
            static: {
                dir: path_dev + '/',
                src: path_dev + '/index.{html,php}',
                dest: path_web
            }
        };

// Javascripts
gulp.task('es-lint', function() {

	return gulp.src(paths.js.s_src)
            .pipe(plugin.eslint())
            .pipe(plugin.eslint.format())
            .pipe(plugin.eslint.failAfterError());
});
gulp.task('js-core', function() {

    return gulp.src([
                paths.js.p_src,
                paths.js.s_src
            ])
		    .pipe(plugin.sourcemaps.init())
            .pipe(plugin.uglify())
            .pipe(plugin.concat('core.js', {newLine: '\n\n'}))
		    .pipe(plugin.sourcemaps.write('.'))
            .pipe(gulp.dest(paths.js.dest));
});
gulp.task('js-libs', function() {

    return gulp.src(paths.js.l_src)
            .pipe(gulp.dest(paths.js.dest + '/libs')); // Use: fallback for CDN
});
gulp.task('js', ['es-lint', 'js-core', 'js-libs']);

// Stylesheets
gulp.task('css', function() {

	return gulp.src(paths.css.src)
		    .pipe(plugin.sourcemaps.init())
            .pipe(plugin.sass({outputStyle: 'compressed'}).on('error', plugin.sass.logError))
            .pipe(plugin.autoprefixer({ browsers: config.autoprefixer }))
            .pipe(plugin.rename('core.css'))
		    .pipe(plugin.sourcemaps.write('.'))
            .pipe(gulp.dest(paths.css.dest));
});

// Fonts
gulp.task('font', function() {

    return gulp.src(paths.font.src)
            .pipe(gulp.dest(paths.font.dest)); // Optional: base64 encoded fonts
});

// Images
gulp.task('img', function() {

    var tinyimages = plugin.filter(paths.img.src + '.{png,jpg,jpeg}', { restore: true });

    return gulp.src(paths.img.src)
		    .pipe(tinyimages)
            .pipe(plugin.tinypngCompress({
                key: config.tinypngKey,
                sigFile: paths.img.dir + '.tinypng-sigs',
                summarise: true
            }))
            .pipe(tinyimages.restore)
            .pipe(gulp.dest(paths.img.dest));
});

// Static
gulp.task('static', function() {

    return gulp.src(paths.static.src)
            .pipe(gulp.dest(paths.static.dest)); // Optional: templating
});

// Watch for changes
gulp.task('watch', function() {

    gulp.watch([paths.js.src, paths.css.src, paths.font.src, paths.img.src, paths.static.src], ['js', 'css', 'font', 'img', 'static']);
 });

 // Default task
gulp.task('default', [], function() {

	return gulp.start('js', 'css', 'font', 'img', 'static');
});