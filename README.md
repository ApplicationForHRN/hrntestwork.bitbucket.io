# Project HRN test

## Directory

This is a simple setup. Only add, delete or modify files in the `dev` folder. The `web` directory contains the compiled, ready to deploy version of the project.

## Process

In order to create the compiled version, use the dedicated Gulp functions. In order to do that:

1. Be sure that you have all dependencies

    - Global: Install Node.js on the computer - [node.js download](https://nodejs.org/en/download/)
    - Global: Install Gulp by opening a command prompt and running `npm install --global gulp-cli`
    - Project: Enter the project folder in cmd and run `npm install`, this will install all the dependencies to the `node_modules` folder regarding the project based on the `package.json`.

2. Run any of the following Gulp functions (`gulpfile.js`) in order to compile:

    - `gulp` - default, run all the tasks
    - `gulp watch` - compiles changed files on the fly
    - `gulp js` - checks and compiles all the js files
    - `gulp js-core` - compiles js plugins and scripts
    - `gulp js-libs` - compiles js libraries
    - `gulp es-lint` - checks js files by `eslint`
    - `gulp css` - compiles scsss into css (with autoprefix)
    - `gulp font` - compiles fonts (without encoding)
    - `gulp img` -  compiles fonts (with optimization)
    - `gulp static` -  compiles static files like html
